%define SYSCALL_READ 0x0
%define SYSCALL_WRITE 0x1
%define SYSCALL_EXIT 0x3c
%define STDIN 0x0
%define STDOUT 0x1


section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov	rax, SYSCALL_EXIT
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor	rax, rax
	.loop:
	cmp	byte [rdi+rax], 0
	je	.end
	inc	rax
	jmp	.loop
	.end:
	ret 

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push	rdi
	call	string_length
	pop	rsi

	mov	rdx, rax
	mov	rax, SYSCALL_WRITE
	mov	rdi, STDOUT
	syscall

	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov     dil, `\n`

; Принимает код символа и выводит его в stdout
print_char:
	push	di

	mov	rsi, rsp
	mov	rax, SYSCALL_WRITE
	mov	rdi, STDOUT
	mov	rdx, 1
	syscall

	add	rsp, 2
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test	rdi, rdi
	jns	print_uint
	
	push	rdi
	mov	dil, '-'
	call	print_char
	pop	rdi
	neg	rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov	rax, rdi
	lea	rdi, [rsp-1]
	sub	rsp, 24

	mov	rcx, 10
	mov	byte [rdi], 0
	.loop:
	xor	rdx, rdx
	div	rcx

	add	dl, '0'
	dec	rdi
	mov	[rdi], dl

	test	rax, rax
	jnz	.loop

	.end:
	call	print_string
	add	rsp, 24
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor	rax, rax
	xor	rcx, rcx

	.loop:
	mov	dl, [rdi+rcx]
	cmp	dl, [rsi+rcx]
	jne	.fail

	inc	rcx

	test	dl, dl
	jnz	.loop
	
	inc	rax
	.fail:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push	0

	mov	rax, SYSCALL_READ
	mov	rdi, STDIN
	mov	rsi, rsp
	mov	rdx, 1
	syscall

	pop	rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push	rbx
	push	r12
	push	r13

	mov	rbx, rdi
	mov	r12, rsi
	xor	r13, r13

	.loop:
	call	read_char
	cmp	al, ' '
	je	.ws
	cmp	al, `\n`
	je	.ws
	cmp	al, `\t`
	je	.ws

	.write:
	cmp	r13, r12
	jae	.fail

	mov	[rbx+r13], al
	test	al, al
	jz	.end
	inc	r13
	jmp	.loop

	.ws:
	test	r13, r13
	jz	.loop
	xor	al, al
	jmp	.write

	.fail:
	xor	rbx, rbx
	xor	r13, r13

	.end:
	mov	rax, rbx
	mov	rdx, r13

	pop	r13
	pop	r12
	pop	rbx
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor	rcx, rcx
	xor	rax, rax
	xor	r8, r8
	mov	r9, 10

	.loop:
	mov	r8b, [rdi+rcx]
	sub	r8b, '0'
	jl	.end
	cmp	r8b, 9
	jg	.end
	inc	rcx

	mul	r9
	add	rax, r8
	jmp	.loop

	.end:
	mov	rdx, rcx
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	mov	cl, [rdi]
	cmp	cl, '-'
	je	.signed
	cmp	cl, '+'
	jne	parse_uint

	.signed:
	inc	rdi
	push	rcx
	call	parse_uint
	pop	rcx

	test	rdx, rdx
	jz	.end

	inc	rdx
	cmp	cl, '-'
	jne	.end
	neg	rax

	.end:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor	rax, rax
	.loop:
        cmp     rax, rdx
        jae     .fail

	mov	cl, [rdi+rax]
	mov	[rsi+rax], cl
	inc	rax
	test	cl, cl
	jnz	.loop

	ret

	.fail:
	xor	rax, rax
	ret

; vim:ts=8:sw=8:et:ft=nasm
